import axios from 'axios';


export default axios.create({
    baseUrl: 'http://61d5a554.ngrok.io',
    'Content-Type': "application/x-www-form-urlencoded",
    Accept: "application/json"
});
